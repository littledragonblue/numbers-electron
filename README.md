# [<img src="https://samoreira.eu/assets/icon/icon_numbers.svg" align="left" width="32px" style="padding-right:10"/> Numbers Game ](https://samoreira.eu/games/?numbers)

[<img src="https://upload.wikimedia.org/wikipedia/commons/9/91/Electron_Software_Framework_Logo.svg" align="right" width="128px">](https://electronjs.org)


> Electron is an open-source framework for creating desktop apps using web technologies. It combines the Chromium rendering engine and the Node.js runtime.

> This app was build to start learn about electron technologie.

<br>

# Documentation
[Electron API](https://www.electronjs.org/docs/v14-x-y/api/app)

# Start App
```
npm start
```

# Install Dependencies
```
npm install electron-packager -g
npm install electron -g
npm install electron-winstaller -g
npm install electron-installer-debian -g
npm install electron-installer-dmg -g
npm install electron-forge -g
npm install parse-url
npm install uuid
```

# Error on Debian
<code>Building electron linux distro : The SUID sandbox helper binary was found, but is not configured correctly:</code>
```
sudo sysctl kernel.unprivileged_userns_clone=1
```

# Author

* [Sérgio Moreira](https://samoreira.eu)