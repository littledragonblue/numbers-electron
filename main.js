const electron = require('electron')
const url = require('url');
const os = require('os');
const fs = require("fs");
const path = require('path');

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true;
process.env.NODE_ENV = 'production'; // Set ENV

const isMac = process.platform === 'darwin';
const { app, BrowserWindow, Menu, ipcMain } = electron;
const configDir = path.join(app.getPath('appData'), "NumbersGame");

if (!fs.existsSync(configDir)) {
    fs.mkdirSync(configDir);
}

let datetime = new Date();
let i18n = new (require(path.join(__dirname, './assets/lang/i18n')));
let username = getUsername();
let score = getScore();
let mainWindow;
let aboutWindow;
process.env.npm_package_version = app.getVersion();

// Listen  for app ti be ready
app.on('ready', function () {
    // Create new windows
    mainWindow = new BrowserWindow({
        roundedCorners: true,
        darkTheme: true,
        shadow: true,
        icon: path.join(__dirname, 'assets/icons/png/64x64.png'),
        // For Electron version 12 and above
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true
        }
    });

    // Load html into window
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'mainWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Quit app when closed
    mainWindow.on('closed', function () {
        app.quit();
    });

    // Build menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

    // Insert menu
    Menu.setApplicationMenu(mainMenu);

    sendData();
    receiveData();
});

// Create menu template
const mainMenuTemplate = [
    {
        label: i18n.__('File'),
        submenu: [
            {
                label: i18n.__('NewGame'),
                click() {
                    mainWindow.webContents.send('setNewGame', true);
                }
            },
            {
                label: i18n.__('SetUsername'),
                click() {
                    createAddUsernameWindow();
                }
            },
            {
                label: i18n.__('Reload'),
                click() {
                    app.relaunch();
                    app.exit();
                }
            },
            {
                label: i18n.__('Exit'),
                accelerator: isMac ? 'Command+Q' : 'Ctrl+Q',
                click() {
                    app.quit();
                }
            }
        ]
    },
    {
        label: i18n.__('Language'),
        submenu: [
            {
                label: 'Português',
                click() {
                    writeConfigFile('language.conf', 'pt');
                    process.env.LANGUAGE = 'pt';
                    app.relaunch();
                    app.exit();
                }
            },
            {
                label: 'Español',
                click() {
                    writeConfigFile('language.conf', 'es');
                    process.env.LANGUAGE = 'es';
                    app.relaunch();
                    app.exit();
                }
            },
            {
                label: 'English',
                click() {
                    writeConfigFile('language.conf', 'en');
                    process.env.LANGUAGE = 'en';
                    app.relaunch();
                    app.exit();
                }
            },
            {
                label: 'Dutch',
                click() {
                    writeConfigFile('language.conf', 'nl');
                    process.env.LANGUAGE = 'nl';
                    app.relaunch();
                    app.exit();
                }
            },
            {
                label: 'Français ',
                click() {
                    writeConfigFile('language.conf', 'fr');
                    process.env.LANGUAGE = 'fr';
                    app.relaunch();
                    app.exit();
                }
            },
            {
                label: 'Deutsch',
                click() {
                    writeConfigFile('language.conf', 'de');
                    process.env.LANGUAGE = 'de';
                    app.relaunch();
                    app.exit();
                }
            }
        ]

    },
    {
        label: i18n.__('Help'),
        submenu: [
            {
                label: i18n.__('PlayOnline'),
                click: async () => {
                    const { shell } = require('electron');
                    await shell.openExternal('https://samoreira.eu/games/?numbers');
                }
            },
            {
                label: i18n.__('Paypal'),
                click: async () => {
                    const { shell } = require('electron');
                    await shell.openExternal('https://www.paypal.com/donate/?cmd=_s-xclick&hosted_button_id=W8MDFB6PRSC8W&source=app.jogonumeros.electron');
                }
            },
            {
                label: 'Reset',

                click: async () => {
                    process.env.LANGUAGE = null;
                    var rimraf = require("rimraf");
                    rimraf(configDir, function () { console.log("done"); });
                    app.relaunch();
                    app.exit();
                }
            },
            {
                label: i18n.__('About'),
                click() {
                    createAboutWindow();
                }
            }
        ]
    }
];

function sendData() {
    const uuid = getuuid();
    mainWindow.webContents.on('did-finish-load', () => {
        mainWindow.webContents.send('footer', username);
        mainWindow.webContents.send('uuid', uuid);
        if (score) {
            mainWindow.webContents.send('score', score);
        }
    });
}

function writeConfigFile(file, value) {
    fs.writeFileSync(path.join(configDir, file), value, (err) => {
        if (err) {
            writeErrorFile(err);
        }
    });
}

function writeErrorFile(message) {
    fs.appendFile(path.join(configDir, 'error_numbersGame.log'), datetime + ': ' + message, function (err) {
        if (err) throw err;
    });
}

function readConfigFile(file) {
    return fs.readFileSync(path.join(configDir, file), 'utf-8', (err, data) => {
        if (err) {
            writeErrorFile(err);
        }

        return data;
    });
}

function receiveData() {
    ipcMain.on('username:add', (event, data) => {
        writeConfigFile('username.conf', data);
        mainWindow.webContents.send('footer', data);
    });

    ipcMain.on('currentScore', (event, data) => {
        if (data < score) {
            writeConfigFile('score.conf', data);
            mainWindow.webContents.send('score', data);

        }
    });
}

function getUsername() {
    if (!fs.existsSync(path.join(configDir, 'username.conf'))) {
        return os.userInfo().username;
    }
    return readConfigFile('username.conf');
}

function getScore() {
    if (!fs.existsSync(path.join(configDir, 'score.conf'))) {
        return;
    }
    return readConfigFile('score.conf');
}

/**
 * Generate Unique UUID
 */
function getuuid() {
    if (!fs.existsSync(path.join(configDir, 'uuid'))) {
        const uuid = require('uuid');
        const id = uuid.v4();
        writeConfigFile('uuid', id);
        return id;
    }
    return readConfigFile('uuid');
}

// Handle create about window
function createAboutWindow() {
    aboutWindow = new BrowserWindow({
        width: 420,
        height: 300,
        title: i18n.__('About'),
        resizable: false,
        fullscreen: false,
        minimizable: false,
        isMinimizable: false,
        darkTheme: true,
        icon: path.join(__dirname, 'assets/icons/png/info.png'),
        roundedCorners: true,
        shadow: true,
        // For Electron version 12 and above
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true
        },
    });

    // Hide menu bar
    aboutWindow.setMenuBarVisibility(false) //partially

    // Load html into window
    aboutWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'about.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Garbage collection handle
    aboutWindow.on('close', function () {
        aboutWindow = null;
    });

    // receive message from about.html 
    ipcMain.on('open:url', (event, url) => {
        require('electron').shell.openExternal(url);
    });
}

// Handle create about window
function createAddUsernameWindow() {
    addUsernameWindow = new BrowserWindow({
        width: 300,
        height: 200,
        title: i18n.__('SetUsername'),
        resizable: false,
        fullscreen: false,
        minimizable: false,
        isMinimizable: false,
        darkTheme: true,
        roundedCorners: true,
        shadow: true,
        alwaysOnTop: true,
        icon: path.join(__dirname, 'assets/icons/png/user_plus.png'),
        // For Electron version 12 and above
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true
        },
    });

    // Hide menu bar
    addUsernameWindow.setMenuBarVisibility(false) //partially

    // Load html into window
    addUsernameWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'addUsername.html'),
        protocol: 'file:',
        slashes: true
    }));

    addUsernameWindow.webContents.on('did-finish-load', () => {
        addUsernameWindow.webContents.send('username', username);
    });

    // Garbage collection handle
    addUsernameWindow.on('close', function () {
        addUsernameWindow = null;
    });
}

/**
 * On macs, the first element of menu is "Electron"
 * add empty to solve this
 */
// if mac, add empty object to menu
if (isMac) {
    mainMenuTemplate.unshift({});
}

// Add developer tools item if not in production
if (process.env.NODE_ENV !== 'production') {
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [
            {
                label: 'Toggle DevTools',
                accelerator: isMac ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'Reload'
            }
        ]
    })
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the 
    // app when the dock icon is clicked and there are no 
    // other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
});