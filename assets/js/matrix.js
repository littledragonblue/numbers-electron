window.onload = function () {
    var canvas = document.getElementById("canvas");
    effectMatrix(canvas);

    function effectMatrix(canvas) {
        var screen = window.screen;
        var width = (canvas.width = screen.width);
        var height = (canvas.height = screen.height);
        var letters = Array(256).join(1).split("");

        var designMatrix = function () {
            canvas.getContext("2d").fillStyle = "rgba(0,0,0,.7)";
            canvas.getContext("2d").fillRect(0, 0, width, height);
            canvas.getContext("2d").fillStyle = "#" + Math.floor(Math.random() * 999);

            letters.map(function (position_y, index) {
                var text = Math.floor(Math.random() * 999999);
                var position_x = index * 10;
                canvas.getContext("2d").fillText(text, position_x, position_y + 10);

                letters[index] = position_y > Math.random() * 1e5 ? 0 : position_y + 10;
            });
        }
        setInterval(designMatrix, 60);
    }
}