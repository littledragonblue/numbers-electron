/**
 * Translation
 * 
 * Why i18n?
 * https://en.wikipedia.org/wiki/Internationalization_and_localization
 * 
 * Toturial:
 * https://www.christianengvall.se/electron-localization/
 */
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true;

const path = require("path")
const electron = require('electron')
const fs = require('fs');
let loadedLanguage;
let app = electron.app ? electron.app : electron.remote.app
const configDir = path.join(app.getPath('appData'), 'NumbersGame');
const language = getLang();
process.env.LANGUAGE = language;

module.exports = i18n;

function i18n() {
    if (fs.existsSync(path.join(__dirname, language))) {
        loadedLanguage = JSON.parse(fs.readFileSync(path.join(__dirname, language), 'utf8'))
    }
    else {
        loadedLanguage = JSON.parse(fs.readFileSync(path.join(__dirname, 'en'), 'utf8'))
    }
}

i18n.prototype.__ = function (phrase) {
    let translation = loadedLanguage[phrase]
    if (translation === undefined) {
        translation = phrase
    }
    return translation
}

function getLang() {
    if (fs.existsSync(path.join(configDir, 'language.conf'))) {
        return fs.readFileSync(path.join(configDir, 'language.conf'), 'utf-8', (err, data) => {
            if (err) {
                fs.appendFile(path.join(configDir, 'error.log'), datetime + ' | Failed to read file! Message: ' + err + "\n");
                return;
            }

            return data;
        });
    } else {
        return (app.getLocale()) ? app.getLocale() : app.getLocaleCountryCode().toLowerCase();
    }
}